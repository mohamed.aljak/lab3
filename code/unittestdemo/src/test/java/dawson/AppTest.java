package dawson;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void echoTest(){
        assertEquals("method echo works",5, App.echo(5));
    }

    @Test
    public void oneMoreTest(){
        assertEquals("method oneMore works", 6, App.oneMore(5), 0);
    }
}
